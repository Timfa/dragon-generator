<?php
class Giantism
{
    var $commonality = 5;

    var $name = "Giantism";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        $dragon->size = ($dragon->size + 7) . "ft";

        return $dragon;
    }
}

RegisterMutation(new Giantism());