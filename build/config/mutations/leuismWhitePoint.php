<?php
class LeuismWhitePoint
{
    var $commonality = 15;

    var $name = "Leuism, White Point";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        return $dragon;
    }
}

RegisterMutation(new LeuismWhitePoint());