<?php
class Dwarfism
{
    var $commonality = 5;

    var $name = "Dwarfism";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        $dragon->size = ($dragon->size - 7) . "ft";

        return $dragon;
    }
}

RegisterMutation(new Dwarfism());