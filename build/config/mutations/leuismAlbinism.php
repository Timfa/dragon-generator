<?php
class LeuismAlbinism
{
    var $commonality = 5;

    var $name = "Leuism, Albinism";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        $dragon->shade = "White";

        return $dragon;
    }
}

RegisterMutation(new LeuismAlbinism());