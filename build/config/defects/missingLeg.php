<?php
class MissingLeg
{
    var $commonality = 5;

    var $name = "MissingLeg";

    function ApplyToDragon($dragon)
    {
        $dragon->geneticDefect = "Missing " . (rand(0, 100) > 50? "front" : "hind") . " leg";

        return $dragon;
    }
}

RegisterDefect(new MissingLeg());