<?php
class Scaleless
{
    var $commonality = 5;

    var $name = "Scaleless";

    function ApplyToDragon($dragon)
    {
        $dragon->geneticDefect = "No scales, smooth, slimy skin.";

        return $dragon;
    }
}

RegisterDefect(new Scaleless());