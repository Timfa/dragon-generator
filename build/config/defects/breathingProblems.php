<?php
class BreathingProblems
{
    var $commonality = 50;

    var $name = "Breathing Problems";

    function ApplyToDragon($dragon)
    {
        $severityN = rand(0, 100);
		
		if ($severityN < 33)
		{
			$severity = "Mild ";
		}
		else if ($severityN < 66)
		{
			$severity = "";
		}
		else
		{
			$severity = "Severe ";
		}

        $dragon->geneticDefect = $severity . $this->name;

        return $dragon;
    }
}

RegisterDefect(new BreathingProblems());