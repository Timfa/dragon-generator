function StartLoad ()
{
    var min = parseInt($("#minEggs").val());
    var max = parseInt($("#maxEggs").val());

    var mother = $("#mother").val();
    var father = $("#father").val();

    console.log(min, max, mother, father);

    LoadDragons(min, max, mother == "" ? null : mother, father == "" ? null : father);
}

function LoadDragons (min, max, mother, father)
{
    $("#dragonContainer").html("");
    //Get all the dragons!
    getDragons(min, max, 5, mother, father, function (data) //function started then dragons are received
    {
        console.log(data); //first log the received data in the console.

        var dragons = data.dragons; //make variable for easy-access to dragons.

        for (var i = 0; i < dragons.length; i++) //loop through all dragons
        {
            logDragonData(dragons[i]);
            displayDragon(dragons[i]);
        }

        setupEvents(); //see Styling.js
    },
        function (data)
        {
            console.log("Request failed!", data);
        });
}

function displayDragon (dragon) //This is where we handle each individual dragon.
{
    if (dragon.dud)
    {
        return; //nothing else here, continue to next dragon. ("return" immediately stops this function, nothing below will be executed.)
    }

    //add it to the HTML for shits 'n giggles
    var newElement = $('<div class="dragon"></div>').text(dragon.color + " Dragon"); //make a new element to put shit in

    $(newElement).append('<div id="content"></div>');

    var contentElement = $(newElement).find("#content");

    $(contentElement).append('<span id="shade"></span>'); //add a span where we'll insert the color

    $(contentElement).find("#shade").text("Shade: " + dragon.shade); //actually put in the color

    $(contentElement).append('<span id="build"></span>'); //add a span where we'll insert the build

    $(contentElement).find("#build").text("Build: " + dragon.build); //actually put in the build

    $(contentElement).append('<span id="gender"></span>'); // add span to insert gender

    $(contentElement).find("#gender").text("Gender: " + dragon.gender); //actually put in gender

    $(contentElement).append('<span id="size"></span>'); //add a span to insert size

    $(contentElement).find("#size").text("Size: " + dragon.size); //actually put in the size

    $(contentElement).append('<span id="wingSpan"></span>'); // add span to insert wingspan

    $(contentElement).find("#wingSpan").text("Wingspan: " + dragon.wingSpan); //actually put in wingspan

    $(contentElement).append('<span id="wingshape"></span>'); // add span to insert wingspan

    $(contentElement).find("#wingshape").text("Wingshape: " + dragon.wingshape); //actually put in wingspan

    if (dragon.mutation) //Does dragon.mutation exist?
    {
        $(contentElement).append('<span id="mutation"></span>'); // add span to insert mutation

        $(contentElement).find("#mutation").text("Mutation: " + dragon.mutation); //actually put in mutation
    }

    if (dragon.secondColor) //does dragon.secondColor exist?
    {
        $(contentElement).append('<span id="secondColor"></span>'); // add span to insert mutation

        $(contentElement).find("#secondColor").text(dragon.secondColor); //actually put in mutation
    }

    if (dragon.thirdColor) //Does dragon.thirdColor exist?
    {
        $(contentElement).append('<span id="thirdColor"></span>'); // add span to insert mutation

        $(contentElement).find("#thirdColor").text(dragon.thirdColor); //actually put in mutation   
    }

    if (dragon.geneticDefect) //Does dragon.defect exist?
    {
        $(contentElement).append('<span id="defect"></span>'); // add span to insert mutation

        $(contentElement).find("#defect").text("Defect: " + dragon.geneticDefect); //actually put in mutation
    }

    $(contentElement).append('<span id="geneticstring"></span>'); // add span to insert wingspan

    $(contentElement).find("#geneticstring").text("Genetic String: " + dragon.geneticString); //actually put in wingspan

    $("#dragonContainer").append(newElement); //put it in the body!
}

function logDragonData (dragon)
{
    console.log(dragon); //log it!

    if (dragon.dud)
    {
        console.log("This egg is dead yo");
        return; //nothing else here, continue to next dragon. ("return" immediately stops this function, nothing below will be executed.)
    }

    console.log(dragon.gender); //log a specific parameter

    if (dragon.clutchSizes) //Check if dragon actually has the parameter before we mess with it! Some parameters might not always be there.
    {
        console.log(dragon.clutchSizes);
    }

    if (dragon.mutation) //Does the dragon have a mutation?
    {
        console.log(dragon.mutation);

        switch (dragon.mutation)
        {
            /***
             * Here we switch through the possible mutation types and act on them.
             * Note that you could treat secondColor etc. like a normal optional parameter (like clutchsizes) too, if you prefer that.
             */
            case "Dual-Colored":
                console.log(dragon.secondColor);
                break;

            case "Tri-Colored":
                console.log(dragon.secondColor);
                console.log(dragon.thirdColor);
                break;

            case "Gender-Swap":
                console.log("http://www.tumblr.com/");
                break;
        }
    }
}

/***
 * amountMin/Max: amount of dragons to generate. Set to "null" to use default.
 * 
 * mutationChance: Chance that a dragon will carry mutations. Set to "null" for default.
 * 
 * callback: the function that will be called (with dragons array as parameter)
 *           when the call is complete.
 * 
 * callbackFail: same as callback, but called when the request fails.
 * 
 * context: Used when calling this from an object instance to preserve context (this)
 *          for example:
 * 
 *          getDragons(complete, this);
 *          would ensure that when getting "this" in complete(), "this" still refers 
 *          to your object. When not calling from an object, this variable will do very little.
 *          (if so, just leave it blank)
 */
function getDragons (amountMin, amountMax, mutationChance, mother, father, callback, callbackFail, context) 
{
    var extraParams = "";

    if (amountMin && amountMax)
    {
        extraParams += "?amountmin=" + Math.min(amountMin, amountMax);
        extraParams += "&amountmax=" + Math.max(amountMin, amountMax);
    }

    if (mutationChance)
    {
        extraParams += ((amountMin && amountMax) ? "&" : "?") + "mutationchance=" + mutationChance;
    }

    if (mother)
    {
        extraParams += (extraParams != "" ? "&" : "?") + "mother=" + mother;
    }

    if (father)
    {
        extraParams += (extraParams != "" ? "&" : "?") + "father=" + father;
    }

    $.ajax({
        url: "api.php" + extraParams,
        dataType: "json",
        success: function (data)
        {
            callback && callback.call(context, data);
        },
        error: function (jqXHR, status, error)
        {
            callbackFail && callbackFail({ jqXHR: jqXHR, status: status, error: error });
        }
    });
}