<?php

/**
  *	DragonGenerator API
  *	By Tim Falken
  */

header("Access-Control-Allow-Origin: *");
header("Content-Type:application/json");

$config = json_decode(file_get_contents("config/config.json"));

$dragons = [];
$errors = [];
$amount = "random";

$mutationChance = 100 - $config->mutationChancePercentage;

$mutationTypes = [];
$defectTypes = [];

$dragonTypes = [];

for($i = 0; $i < count($config->includes->dragons); $i++)
{
	$dragonType = json_decode(file_get_contents("config/dragontypes/" . $config->includes->dragons[$i]));
	array_push($dragonTypes, $dragonType);
}

for($i = 0; $i < count($config->includes->mutations); $i++)
{
	require_once("config/mutations/" . $config->includes->mutations[$i]);
}

for($i = 0; $i < count($config->includes->defects); $i++)
{
	require_once("config/defects/" . $config->includes->defects[$i]);
}

if (isset($_GET["mutationchance"])) //default: 5%
{
    $mutationChance = 100 - $_GET["mutationchance"];
}

if (isset ($_GET["amount"]))
{
    $amount = $_GET["amount"];
}

if (isset ($_GET["amountmin"]))
{
    $config->amountOfDragons->min = $_GET["amountmin"];
}

if (isset ($_GET["amountmax"]))
{
    $config->amountOfDragons->max = $_GET["amountmax"];
}

if ($amount == "random")
    $amount = rand($config->amountOfDragons->min, $config->amountOfDragons->max);

for ($i = 0; $i < $amount; $i++)
{
    array_push($dragons, BuildDragon($mutationChance));
}

if(count($errors) == 0)
{
    $result = new stdClass();

    $result->info = new stdClass();
    $result->info->amount = (int)$amount;
    $result->info->mutationChance = 100 - $mutationChance;

	$result->parents = new stdClass();
	$result->parents->mother = GetMother();
	$result->parents->father = GetFather();
	
	if (isset($result->parents->father->mutation))
	{
		$result->parents->father->mutation = $result->parents->father->mutation->type;
	}
	
	if (isset($result->parents->mother->mutation))
	{
		$result->parents->mother->mutation = $result->parents->mother->mutation->type;
	}
	
    $result->dragons = $dragons;

	$result->about = new stdClass();

	$result->about->author = "Tim Falken";
	$result->about->website = "http://www.timfalken.com/";

	$result->about->config = new stdClass();
	$result->about->config->name = $config->configName;
	$result->about->config->author = $config->configAuthor;

    echo json_encode($result);
}
else
{
    $ErrObj = new stdClass();

    $ErrObj->errors = $errors;

    echo json_encode($ErrObj);
}

function AddError($errors, $type, $message)
{
    $newError = new stdClass();

    $newError->type = $type;
    $newError->message = $message;

    array_push($errors, $newError);
}

function GetMother()
{
	return GetParent("mother");
}

function GetFather()
{
	return GetParent("father");
}

function GetParent($parent)
{
	if ( isset($_GET[$parent]) )
	{
		$result = json_decode(base64_decode($_GET[$parent]));
		$result->geneticString = $_GET[$parent];
		return $result;
	}
	else
	{
		return "unspecified";
	}
}

function BuildDragon($mutationChance)
{
	global $dragonTypes, $mutationTypes, $defectTypes;

    $newDragon = new stdClass();

	$indexes = [];

	for ($i = 0; $i < count($dragonTypes); $i++)
	{
		for ($a = 0; $a < $dragonTypes[$i]->commonality; $a++)
			array_push($indexes, $i);
	}

	$type = $dragonTypes[RandomElement($indexes)]; //effectively a weighted random.

	$newDragon->gender = $type->male? "Male" : "Female";
	$newDragon->clutchSizes = $type->clutchSizes->min . "-" . $type->clutchSizes->max;
	$newDragon->color = $type->color;
	$newDragon->shade = RandomElement($type->shades);
	$newDragon->build = RandomElement($type->builds);
	$newDragon->wingshape = RandomElement($type->wingshapes);

	$newDragon->size = rand($type->size->min, $type->size->max) . "ft";
	
	$newDragon = TryInherit($newDragon);
	
    if (rand(0, 100) > $mutationChance && !isset($newDragon->mutation))
    {
		$newDragon->mutation = new stdClass();
		
		$indexes = [];

		for ($i = 0; $i < count($mutationTypes); $i++)
		{
			for ($a = 0; $a < $mutationTypes[$i]->commonality; $a++)
				array_push($indexes, $i);
		}

		$mutationIndex = RandomElement($indexes);

		$mutationType = $mutationTypes[$mutationIndex]; //effectively a weighted random.

		$newDragon->mutation->index = $mutationIndex;

		$newDragon = $mutationType->ApplyToDragon($newDragon);
    }

    if (isset($newDragon->size) && !isset($newDragon->wingSpan))
    {
        $newDragon->wingSpan = GetWingspan($newDragon);
    }

	if ($newDragon->gender == "Male")
	{
		unset($newDragon->clutchSizes);
	}
	else if ($newDragon->clutchSizes == "0-0")
	{
		$newDragon->clutchSizes = "Sterile";
	}

	$newDragon->geneticString = base64_encode(json_encode($newDragon));
	
	if (isset($newDragon->mutation))
	{
		$newDragon->mutation = $newDragon->mutation->type;
	}
	
    return $newDragon;
}

function GetWingspan($dragon)
{
	$ds = str_replace("ft", "", $dragon->size);
	return round((($ds + 1) * (rand(150, 170) / 100))) . "ft";
}

function TryInherit($dragon)
{
	global $mutationTypes;

	$mother = GetMother();
	$father = GetFather();
	
	$motherMutation = null;
	$fatherMutation = null;

	$ds = str_replace("ft", "", $dragon->size);
	$dws = str_replace("ft", "", GetWingspan($dragon));

	if($mother != "unspecified" && $father != "unspecified")
	{
		$fs = str_replace("ft", "", $father->size);
		$ms = str_replace("ft", "", $mother->size);
		$fws = str_replace("ft", "", $father->wingSpan);
		$mws = str_replace("ft", "", $mother->wingSpan);

		$avgSize = (((($ms + $fs) / 2) + $ds) / 2) . "ft";
		$avgSpan = (((($mws + $fws) / 2) + $dws) / 2) . "ft";
	}
	else if($mother != "unspecified")
	{
		$ms = str_replace("ft", "", $mother->size);
		$mws = str_replace("ft", "", $mother->wingSpan);

		$avgSize = (($ms + $ds) / 2) . "ft";
		$avgSpan = (($mws + $dws) / 2) . "ft";
	}
	else if($father != "unspecified")
	{
		$fs = str_replace("ft", "", $father->size);
		$fws = str_replace("ft", "", $father->wingSpan);

		$avgSize = (($fs + $ds) / 2) . "ft";
		$avgSpan = (($fws + $dws) / 2) . "ft";
	}
	else
	{
		return $dragon;
	}
	
	$dragon->size = round(str_replace("ft", "", $avgSize)) . "ft";
	$dragon->wingSpan = round(str_replace("ft", "", $avgSpan)) . "ft";
	
	if ($mother != "unspecified")
	{
		if (isset($mother->mutation))
		{
			$motherMutation = $mother->mutation;
		}
	}
	
	if ($father != "unspecified")
	{
		if (isset($father->mutation))
		{
			$fatherMutation = $father->mutation;
		}
	}
	
	if ($motherMutation == null && $fatherMutation == null)
	{
		return $dragon;
	}
	
	$defectChance = 0;
	
	if ($motherMutation == $fatherMutation)
	{
		$totalInherited = 0;
		
		if (isset($motherMutation->timesInherited))
			$totalInherited += $motherMutation->timesInherited;
			
		if (isset($fatherMutation->timesInherited))
			$totalInherited += $fatherMutation->timesInherited;
			
		$chance = InheritChance($totalInherited);
		
		if(rand(0, 100) < max($chance, 10))
		{
			$dragon->mutation = $motherMutation;
			$dragon->mutation->timesInherited = ceil($totalInherited / 2) + 1;
		}
		
		$defectChance = $chance;
	}
	else
	{
		$motherInherited = -1;
		$fatherInherited = -1;
	
		if ($motherMutation != null)
		{		
			if (isset($motherMutation->timesInherited))
				$motherInherited = $motherMutation->timesInherited;
			else
				$motherInherited = 0;
		}
		
		if ($fatherMutation != null)
		{		
			if (isset($fatherMutation->timesInherited))
				$fatherInherited = $fatherMutation->timesInherited;
			else
				$fatherInherited = 0;
		}
		
		$chosenMutation = $fatherInherited > $motherInherited? $fatherMutation : $motherMutation;
		
		$inheritedTimes = 0;
		
		if (isset($chosenMutation->timesInherited))
		{
			$inheritedTimes = $chosenMutation->timesInherited;
		}
		
		$chance = InheritChance($inheritedTimes);
		
		if(rand(0, 100) < max(10, $chance))
		{
			$dragon->mutation = new stdClass();

			$mutation = $mutationTypes[$chosenMutation->index];

			$dragon = $mutation->ApplyToDragon($dragon);
			$dragon->mutation->index = $chosenMutation->index;

			$dragon->mutation->timesInherited = ceil($inheritedTimes) + 1;
		}
		
		$defectChance = $chance;
	}
	
	$dragon = TryDefect($dragon, $defectChance);
	
	return $dragon;
}

function TryDefect($dragon, $chance)
{
	global $defectTypes;

	if (rand(0, 100) < $chance)
	{	
		$indexes = [];

		for ($i = 0; $i < count($defectTypes); $i++)
		{
			for ($a = 0; $a < $defectTypes[$i]->commonality; $a++)
				array_push($indexes, $i);
		}

		$defect = $defectTypes[RandomElement($indexes)]; //effectively a weighted random.

		$dragon = $defect->ApplyToDragon($dragon);
	}

	return $dragon;
}

function InheritChance($e)
{
	return (1 - (1 / Exp($e / 5))) * 100;
}

function RandomElement($shades)
{
    return $shades[mt_rand(0, count($shades) - 1)];
}

function RegisterMutation($mutationObject)
{
	global $mutationTypes;
	array_push($mutationTypes, $mutationObject);
}

function RegisterDefect($defectObject)
{
	global $defectTypes;
	array_push($defectTypes, $defectObject);
}